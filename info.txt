PREMISE
	-Anonymous quote posting service. Post a quote and have it viewed and rated by strangers. Strangers have no idea you wrote it
	-Only you can view your own quotes, all other quotes are anonymous.

SERVER DEV TODO:
DONE	-Login/Register System
DONE	-Quotes	(Post and Rate)
			-Cannot rate more than once
DONE	-Users can see list of all previous quotes and 'likes'/'dislikes'
DONE	-Anonymous quote posting and whatnot.
DONE	-View random quotes, like/dislike, purposely hide who posted it.
	-any user can: (No login required)
		-view top viewed/ liked quotes
DONE	-See all quotes 
PARTIAL	-Implement Routes (see routes.txt)
		
Important Information:
OFFICIAL PARSE
	user: flowe1@hawk.iit.edu
	pass: 4p#gqyba
	
Current Issue:
How to work the rating system
	Like/Dislike?
		Adds complexity, but really lets you see the bad quotes.
	Or just Like?
		Makes sorting way easier. And coding too.
	
Sidenotes:
	Avoid using 'str' as a variable name. It's python's string class name, and leads to severe confusion
	
CHANGELOG
Hugo - 11/8/2013
Added a quotes.html template.
Added basic like/dislike ability.
Users can see likes and dislikes
Users can now see quote by ID.
Short term now: View quotes by total likes/largest like/dislike ratio

Hugo - 11/7/2013
Major update, added ability to edit your password.
Added more quote processing things:
	view own quotes
	view all posted quotes
	working on a quicksort to sort by likes, will merge with webApp when finished.
user functions have been changed a bit, as well as login and registration
Short term goals now: Get a like/dislike system working.

Hugo - 11/6/2013
Edited the HTML files to match the suggested routes
restructured some things
Organized All routes a little better, more coherent and whatnot.
Added important quote methods
Laid Groundwork for future quote processing 